# Led_Erreur404

Here is the code used for the control of led strips, during Erreur404 crazy night.

Check Erreur404: https://www.facebook.com/erreur404crew/

Check description of the project: FIXME

## Getting Started

It is a Platformio project, you can directly import it in the IDE, or use your prefered IDE.

### Prerequisites

To install and use Platformio, you have several options. Personnally, I use it in the Atom text editor.

In Linux, you can install atom with the following command:
```
sudo apt-get install atom
```

And directly install the Platformio plugin in Edit > Preferences > Install. Search for official platformio-ide package.

Official notes: https://docs.platformio.org/en/latest/ide/atom.html

## Built with

* Arduino framework

## Authors

* Pierre Lemasson - https://www.pierre-lemasson.com

## Licence

This is totally free and open source, I don't know a lot about licences, see platformio licences, Arduino framework licences.

