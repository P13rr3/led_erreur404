#include "main.hpp"

#include "Global.hpp"


LightAnimator* myLight;
SoundManager* soundmanager;
uint8_t animationCounter;

// Interrupt variable
volatile int pinInterruptCounter = 0;
int numberOfInterrupts = 0;
portMUX_TYPE interruptMux = portMUX_INITIALIZER_UNLOCKED;

// Timer variables
volatile int timerInterruptCounter;
int totalInterruptCounter;
hw_timer_t* timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  timerInterruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

void IRAM_ATTR handleInterrupt()
{
  portENTER_CRITICAL_ISR(&interruptMux);
  pinInterruptCounter++;
  portEXIT_CRITICAL_ISR(&interruptMux);
}

// Special Task
void coreDataTask( void * parameter )
{
  while(1)
  {
    soundmanager->update();
    delay(30);
  }
}

// This method is called every ten minutes, to change the animation. TODO Should be manage by a Light Manager
void animationChange()
{
  if(animationCounter >= NUMBER_ANIMATIONS) animationCounter = 0;
  else animationCounter++;
}

void setup()
{

  delay(1000);

  // For debug
  Serial.begin(DEBUG_BAUDRATE);

  // Configure the timer, see https://techtutorialsx.com/2017/10/07/esp32-arduino-timer-interrupts/
  // Note: frequency of the ESP32 is 80MHz. The timer counter will be incremented each second
  timer = timerBegin(0, 80,true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 600000000, true);
  timerAlarmEnable(timer);

  // Interrupt configuration
  pinMode(PIN_INTERRUPT, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_INTERRUPT), handleInterrupt, HIGH);

  // Pin config
  pinMode(LED_PIN_DEBUG_1, OUTPUT);
  digitalWrite(LED_PIN_DEBUG_1, 0);

  // Create LightAnimator object
  myLight = new LightAnimator();

  // Create SoundManager object
  soundmanager = new SoundManager();

  // Activate the soundmanager
  soundmanager->setActive(true);

  animationCounter = 0;

  Serial.println("\n\n");

  // Create the task used to retrieve and analyze data
  xTaskCreatePinnedToCore(
                  coreDataTask,               /* Task function. */
                  "coreDataTask",             /* String with name of task. */
                  10000,                      /* Stack size in bytes. */
                  NULL,                       /* Parameter passed as input of the task */
                  0,                          /* Priority of the task. */
                  NULL,                       /* Task handle. */
                  CORE_DATA_TASK);            /* Core where the task should run */

  Serial.println("coreDataTask created...");
}

// Main task
void loop()
{
  if (timerInterruptCounter)
  {
    portENTER_CRITICAL(&timerMux);
    timerInterruptCounter = 0;
    portEXIT_CRITICAL(&timerMux);
    totalInterruptCounter++;
    animationChange();
    Serial.print("An interrupt as occurred. Total number: ");
    Serial.println(totalInterruptCounter);
  }

  if(pinInterruptCounter > 0)
  {

      portENTER_CRITICAL(&interruptMux);
      pinInterruptCounter--;
      portEXIT_CRITICAL(&interruptMux);

      numberOfInterrupts++;
      Serial.print("An interrupt has occurred. Total: ");
      Serial.println(numberOfInterrupts);
      myLight->strob();
  }

  // TODO in switch cases, make use of totalInterruptCounter to define different animation given the moment of the party
  /*switch (animationCounter)
  {
    case 0:
      if(!pinInterruptCounter) myLight->colorPalette(LightAnimator::PALETTE_OCEAN, 50, soundmanager->getRms(), soundmanager->getSpecCrest());
      break;

    case 1:
      if(!pinInterruptCounter) myLight->noise(soundmanager->getZcr(), soundmanager->getSpecFlatness(), soundmanager->getSpecCentroid(), 130, 255);
      break;

    case 2:
      if(!pinInterruptCounter) myLight->colorPalette(LightAnimator::PALETTE_FRIDAY_SUNSET, 50, soundmanager->getRms(), soundmanager->getSpecCrest());
      break;

    case 3:
      if(!pinInterruptCounter) myLight->fire(soundmanager->getRms(), soundmanager->getSpecCrest());
      break;

    case 4:
      if(!pinInterruptCounter) myLight->colorPalette(LightAnimator::PALETTE_FRENCH_FLAG, 50, soundmanager->getRms(), soundmanager->getSpecCrest());
      break;

    default:
      if(!pinInterruptCounter) myLight->fire(soundmanager->getRms(), soundmanager->getSpecCrest());
      break;
  }*/




  //Serial.print(soundmanager->getRms());
  //Serial.print('\n');
  //myLight->noise(311, 400, 170, 255);
  //myLight->animationTest();
  //myLight->testMathFunction();
  //myLight->moveTwoOpposite(192, 200, soundmanager->getZcr(), soundmanager->getSpecCentroid(), soundmanager->getSpecFlatness());
  if(!pinInterruptCounter) myLight->colorPalette(LightAnimator::PALETTE_OCEAN, 50, soundmanager->getRms(), soundmanager->getSpecCrest());
  yield();
  delay(5);
}
