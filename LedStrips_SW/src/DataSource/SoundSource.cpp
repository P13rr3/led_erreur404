#include "DataSource/SoundSource.hpp"

SoundSource::SoundSource():
  DataSource(),
  dataOK_m(false)
{
  // Instantiate the data table
  datas_m = new char[DATA_BUFFER_LENGTH];
  // Instantiate the serial object
  swSer = new SoftwareSerial();
}

bool SoundSource::init()
{
  // Init Serial connection
  swSer->begin(BAUD_RATE, RX_PIN, TX_PIN, SWSERIAL_8N1, false, BLOCKSIZE * 4);  // Serial communication for datas
  while(!(*swSer))
  {
    // Wait until swSer is ready
    // FIXME possible infinite loop, what to do here ??
  }
  return true;
}

bool SoundSource::getData(char* pData, int length)
{
  bool validity;
  if(length == DATA_BUFFER_LENGTH && dataOK_m)
  {
    for(uint8_t i = 0; i < DATA_BUFFER_LENGTH; i++)
    {
      pData[i] = datas_m[i];
    }
    validity = true;
  }
  else
  {
    pData = nullptr;
    validity = false;
  }
  return validity;
}

void SoundSource::tick()
{
  while (swSer->available() > 0)
  {
    swSer->readBytesUntil(static_cast<char>(END_SIGNAL), &datas_m[0], static_cast<int>(DATA_BUFFER_LENGTH));

    if(datas_m[0] == START_SIGNAL)
    {
      dataOK_m = true;
    }
    else
    {
      dataOK_m = false;
    }
  }
}
