#include "manager/SoundManager.hpp"

SoundManager::SoundManager():
  DataSourceManager(),
  validity_m(false)
{
  source_m = new SoundSource();
  init();
}

void SoundManager::init()
{
  // TODO call the source init
  source_m->init();
}

DataSource::DataSourceID SoundManager::getID()
{
  return DataSource::DATA_SOURCE_SOUND;
}

void SoundManager::update()
{
  if(active_m)
  {
    source_m->tick();
    validity_m = source_m->getData(rawData_m, SoundSource::DATA_BUFFER_LENGTH);
    processData();
  }
  else
  {
    // Function not active, no uupdate
  }
}

int SoundManager::getRms()
{
  return (validity_m ? rms_m : DataSource::ERROR_VALUE);
}

int SoundManager::getZcr()
{
  return (validity_m ? zcr_m : DataSource::ERROR_VALUE);
}

int SoundManager::getPeakNrj()
{
  return (validity_m ? peakNrj_m : DataSource::ERROR_VALUE);
}

int SoundManager::getSpecCentroid()
{
  return (validity_m ? specCentroid_m : DataSource::ERROR_VALUE);
}

int SoundManager::getSpecCrest()
{
  return (validity_m ? specCrest_m : DataSource::ERROR_VALUE);
}

int SoundManager::getSpecFlatness()
{
  return (validity_m ? specFlatness_m : DataSource::ERROR_VALUE);
}

int SoundManager::getRollOff()
{
  return (validity_m ? rollOff_m : DataSource::ERROR_VALUE);
}

int SoundManager::getSpecKurtosis()
{
  return (validity_m ? specKurtosis_m : DataSource::ERROR_VALUE);
}

int SoundManager::getNrjDiff()
{
  return (validity_m ? nrjDiff_m : DataSource::ERROR_VALUE);
}

int SoundManager::getSpecDifference()
{
  return (validity_m ? specDiff_m : DataSource::ERROR_VALUE);
}

int SoundManager::getHfContent()
{
  return (validity_m ? hfContent_m : DataSource::ERROR_VALUE);
}

int SoundManager::getPitch()
{
  return (validity_m ? pitch_m : DataSource::ERROR_VALUE);
}

void SoundManager::processData()
{
  rms_m             = (rawData_m[2] << 8) | rawData_m[1];
  peakNrj_m         = rawData_m[3];
  zcr_m             = (rawData_m[5] << 8) | rawData_m[4];
  specCentroid_m    = (rawData_m[7] << 8) | rawData_m[6];
  specCrest_m       = (rawData_m[9] << 8) | rawData_m[8];
  specFlatness_m    = rawData_m[10];
  rollOff_m         = rawData_m[11];
  specKurtosis_m    = (rawData_m[13] << 8) | rawData_m[12];
  nrjDiff_m         = (rawData_m[15] << 8) | rawData_m[14];
  specDiff_m        = (rawData_m[17] << 8) | rawData_m[16];
  hfContent_m       = (rawData_m[19] << 8) | rawData_m[18];
  pitch_m           = (rawData_m[21] << 8) | rawData_m[20];
}
