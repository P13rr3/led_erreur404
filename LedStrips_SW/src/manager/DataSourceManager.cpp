#include "manager/DataSourceManager.hpp"

DataSourceManager::DataSourceManager():
  active_m(false),
  available_m(true)
{
}

bool DataSourceManager::setActive(bool active)
{
  bool success;
  if(available_m)
  {
    active_m = active;
    success = true;
  }
  else
  {
    success = false;
  }
  return success;
}

bool DataSourceManager::isActive()
{
  return active_m;
}

bool DataSourceManager::isAvailable()
{
  return available_m;
}

int DataSourceManager::getNbAvailableSource()
{
  // FIXME method not working
  int result = 0;
  for(int i = 0; i < DataSource::NB_TOTAL_DATA_SOURCES; ++i)
  {
    if(available_m)
    {
      result++;
    }
  }
  return result;
}
