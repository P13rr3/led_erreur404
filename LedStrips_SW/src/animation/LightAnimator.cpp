#include "animation/LightAnimator.hpp"



/**
 * Pattern :
 * {
 *  Timing, RED, BLUE, GREEN
 * }
 *
 */

DEFINE_GRADIENT_PALETTE( grindylow_21_gp )
{
    0, 241,101,105,
   51,  241,60,240,
  128,  241,26,240,
  191,  182,26,105,
  255,  151,26, 80
};

DEFINE_GRADIENT_PALETTE( purplefly_gp )
{
    0,   0,  0,  0,
   63, 0,  239,122,
  191, 255,252, 78,
  255,   0,  0,  0
};

  DEFINE_GRADIENT_PALETTE( friday_sunset_gp )
  {
      0, 252,  2,  2,
     89, 252,  2,  2,
     89, 132,  1,  6,
    150, 132,  1,  6,
    150,  67,  1, 10,
    168,  67,  1, 10,
    168,  25,  3,  9,
    198,  25,  3,  9,
    198,   2,  3,  2,
    255,   2,  3,  2
  };

DEFINE_GRADIENT_PALETTE( French_flag_gp )
{
    0,   0,  0, 255,
   42,   0,  0, 255,
   84,   0,  0, 255,
   84, 255,255,255,
  127, 255,255,255,
  170, 255,255,255,
  170, 255,  0,  0,
  212, 255,  0,  0,
  250, 255,  0,  0,
  250,   0,  0,  0,
  255,   0,  0,  0
};




const TProgmemRGBGradientPalettePtr gGradientPalettes[]=
{
  grindylow_21_gp,
  purplefly_gp,
  friday_sunset_gp,
  French_flag_gp
};


LightAnimator::LightAnimator():
  currentAnimation_m(NO_ANIMATION)
{
  FastLED.addLeds<WS2811,DATA_PIN, BRG>(leds_m, NUM_LEDS);
}

void LightAnimator::animationTest()
{
  for(uint16_t i = 0; i < NUM_LEDS; i++)
  {
    leds_m[i] = CRGB(0, 0, 0);
  }
  leds_m[0] = CHSV(160,200,200);
  leds_m[2] = CHSV(160,200,200);

  FastLED.show();
  delay(20);
}

void LightAnimator::calibratorTest()
{
  leds_m[0] = CRGB(255,0,0);
  leds_m[1] = CRGB(255,0,0);
  leds_m[2] = CRGB(0,255,0);
  leds_m[3] = CRGB(0,255,0);
  leds_m[4] = CRGB(0,0,255);
  leds_m[5] = CRGB(0,0,255);
  leds_m[6] = CRGB(0,0,0);
  leds_m[7] = CRGB(0,0,0);
  leds_m[8] = CRGB(255,255,255);
  leds_m[9] = CRGB(255,255,255);
  FastLED.show();
  delay(1000);
}


void LightAnimator::fire(uint16_t rms, uint16_t specCrest)
{
  uint8_t heatOffset;
  uint8_t sparking;

  if(rms > 1000) rms = 1000;
  if(specCrest > 1800) specCrest = 1800;

  heatOffset = map(rms, 0, 1000, 10, 255);
  sparking = map(specCrest, 0, 1800, 10, 255);

  currentAnimation_m = ANIMATION_FIRE;
  // Array of temperature readings at each simulation cell
  static byte heat[NUM_LEDS];

  // Step 1.  Cool down every cell a little
  for( int i = 0; i < NUM_LEDS; i++)
  {
    heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
  }

  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for( int k= NUM_LEDS - 1; k >= 2; k--)
  {
    heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
  }

  // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
  if( random8() < sparking )
  {
    int y = random16(NUM_LEDS - 5);
    heat[y] = qadd8( heat[y], heatOffset );
  }

  // Step 4.  Map from heat cells to LED colors
  for( int j = 0; j < NUM_LEDS; j++)
  {
    CRGB color = HeatColor(heat[j]);
    int pixelnumber;
    pixelnumber = j;
    leds_m[pixelnumber] = color;
  }
  FastLED.show();
  delay(50);
}

void LightAnimator::noiseDEP(uint16_t scale, uint16_t speed, uint8_t hueMin, uint8_t hueMax)
{
  currentAnimation_m = ANIMATION_NOISE;
  static const uint16_t kMatrixWidth = 1;
  static const uint16_t kMatrixHeight = NUM_LEDS;

  // The 32bit version of our coordinates
  static uint16_t x = random16();
  static uint16_t y = random16();
  static uint16_t z = random16();
  static uint8_t ihue=0;

  uint16_t MAX_DIMENSION = ((kMatrixWidth>kMatrixHeight) ? kMatrixWidth : kMatrixHeight);

  //----------fillnoise8 function----------------------
  for(int i = 0; i < MAX_DIMENSION; i++)
  {
    int ioffset = scale * i;
    for(int j = 0; j < MAX_DIMENSION; j++)
    {
      int joffset = scale * j;
      noise_m[i][j] = inoise8(x + ioffset,y + joffset,z);
    }
  }
  z += speed;
  //---------------------------------------------------
  for(int i = 0; i < kMatrixWidth; i++) {
    for(int j = 0; j < kMatrixHeight; j++) {
      // We use the value at the (i,j) coordinate in the noise
      // array for our brightness, and the flipped value from (j,i)
      // for our pixel's hue.
      //leds_m[XY(i,j,kMatrixWidth)] = CHSV(noise_m[j][i],255,noise_m[i][j]);

      // You can also explore other ways to constrain the hue used, like below
      leds_m[XY(i,j,kMatrixWidth)] = CHSV(ihue + (noise_m[j][i]>>2),255,noise_m[i][j]);
    }
  }
  if(ihue>hueMax)
  {
    --ihue;
  }
  else if(ihue <= hueMin )
  {
    ++ihue;
  }

  LEDS.show();
  delay(20);
}

void LightAnimator::noise(uint16_t zcr, uint16_t specFlat, uint16_t specCentroid, uint8_t minHue, uint8_t maxHue)
{
  uint16_t _zcr = zcr;
  uint16_t _specFlat = specFlat;
  uint16_t _specCent = specCentroid;
  uint8_t hueOffset;
  uint16_t random1;
  uint16_t random2;
  uint16_t random3;
  uint8_t nbLedTurnedOff = 2;

  if (_zcr > 255) _zcr = 255;
  if(_specFlat > 150) _specFlat = 150;
  if(_specCent > 255) _specCent = 255;

  hueOffset = random8(0, _zcr);

  for(uint16_t i = 0; i < NUM_LEDS; i++)
  {
    leds_m[i] = CHSV(minHue + map(_zcr, 0, 200, 30, maxHue),
                    map(_specCent, 0, 255, 100, 255),
                    130 + random8(0,120));
  }

  // Turn off randomly some LEDs
  random1 = random16( 0, (NUM_LEDS / 3) - nbLedTurnedOff);
  for (uint8_t i = random1; i < random1 + nbLedTurnedOff; i++) leds_m[i] = CRGB(0, 0, 0);

  random2 = random16( (NUM_LEDS / 3), (2 * (NUM_LEDS / 3)) - nbLedTurnedOff);
  for (uint8_t i = random2; i < random2 + nbLedTurnedOff; i++) leds_m[i] = CRGB(0, 0, 0);

  random3 = random16( 2*(NUM_LEDS / 3), NUM_LEDS - nbLedTurnedOff);
  for (uint8_t i = random3; i < random3 + nbLedTurnedOff; i++) leds_m[i] = CRGB(0, 0, 0);


  FastLED.show();
  FastLED.delay(80);
}

void LightAnimator::moveTwoOpposite(uint8_t mainHue, uint16_t _delay, uint16_t zcr, uint16_t specCentroid, uint8_t specFlatness)
{
  // Define the number of led lightned for one element
  uint16_t nbOfLightnedLeds = NUM_LEDS / 4;
  // Define a static index to the first lightned led, to introduce movement
  static uint16_t startIndex = 0;
  // Indicate the direction
  bool moveUp;


  if(startIndex <= NUM_LEDS)
  {
    moveUp = true;
    startIndex++;
  }
  else
  {
    moveUp = false;
    startIndex--;
  }

  for(uint16_t i = 0; i < NUM_LEDS; i++)
  {
    //if(moveUp)
    //{
      if(i < (nbOfLightnedLeds + startIndex))
      {
        leds_m[i + startIndex] = CHSV(mainHue,180,(i*255)/nbOfLightnedLeds);
      }
      else
      {
        leds_m[i] = CRGB(0,0,0);
      }
    /*}
    else
    {
    }*/
  }

  FastLED.show();
  FastLED.delay(_delay);
}

void LightAnimator::colorPalette(E404ColorPalette colorP, uint16_t _delay, int rms, int crest)
{
  uint8_t startIndexOffset = 10;
  uint8_t colorIndexIncrement = 10;
  // By definition, RMS is 0 -> 1000. If > 1000, this is an communication issue, and not a good value
  if(rms > 5000)
  {
    rms = 150;
  }
  //RMS normally between 0 & 1000
  uint8_t brightness = map(rms, 150, 5000, 10, 255);

  currentAnimation_m = ANIMATION_COLOR_PALETTE;
  CRGBPalette16 myPalette;
  TBlendType blendType = LINEARBLEND;
  switch (colorP)
  {
    case PALETTE_OCEAN:
      myPalette = OceanColors_p;
      break;

    case PALETTE_CLOUD:
      myPalette = CloudColors_p;
      break;

    case PALETTE_LAVA:
      myPalette = LavaColors_p; // see above comment
      break;

    case PALETTE_FOREST:
      myPalette = ForestColors_p; // Here bug from lib apparently, forest look like lava, & vice versa
      break;

    case PALETTE_PARTY:
      myPalette = PartyColors_p;
      break;

    case PALETTE_GRINDY_LOW:
      myPalette = grindylow_21_gp;
      break;

    case PALETTE_PURPLE_FLY:
      myPalette = purplefly_gp;
      break;

    case PALETTE_FRIDAY_SUNSET:
      myPalette = friday_sunset_gp;
      break;

    case PALETTE_FRENCH_FLAG:
        myPalette = French_flag_gp;
        break;

    case NO_COLOR_PALETTE:
    default:
      break;
  }
  static uint8_t startIndex = 0;
  startIndex+=startIndexOffset;
  uint8_t colorIndex = startIndex;
  for( int i = 0; i < NUM_LEDS; i++)
  {
    leds_m[i] = ColorFromPalette(myPalette, colorIndex, brightness, blendType);
    colorIndex += colorIndexIncrement;
  }
  FastLED.setBrightness(brightness);
  FastLED.show();
  FastLED.delay(_delay);
}

void LightAnimator::strob()
{
  for(uint8_t i = 0; i < NUM_LEDS; i++)
  {
    leds_m[i] = CRGB(255,255,255);
  }
  FastLED.setBrightness(255);
  FastLED.show();
  FastLED.delay(30);

  ledTurnOff();
  FastLED.delay(50);
}

uint16_t LightAnimator::XY( uint8_t x, uint8_t y,  uint8_t matrixWidth)
{
  const bool    kMatrixSerpentineLayout = true;
  uint16_t i;

  if( kMatrixSerpentineLayout == false)
  {
    i = (y * matrixWidth) + x;
  }

  if( kMatrixSerpentineLayout == true)
  {
    if( y & 0x01)
    {
      // Odd rows run backwards
      uint8_t reverseX = (matrixWidth - 1) - x;
      i = (y * matrixWidth) + reverseX;
    }
    else
    {
      // Even rows run forwards
      i = (y * matrixWidth) + x;
    }
  }
  return i;
}

void LightAnimator::testMathFunction()
{
  for(int i = 0; i < NUM_LEDS; ++i)
  {
    leds_m[i] = CHSV(200,200,225);
  }
  leds_m[0].nscale8(25);
  leds_m[1].nscale8(30);

  leds_m[3] = CHSV(96,200,255);

  leds_m[3]>>=5;

  /*for(int i = 4; i < NUM_LEDS; ++i)
  {
    leds_m[i].fadeToBlackBy(3*(i*255)/NUM_LEDS);
  }*/

  for(int i = 0; i < NUM_LEDS; i++)
  {
    leds_m[i] = CHSV(224, 200, triwave8((i*3*255)/NUM_LEDS));
  }

  FastLED.show();

  delay(50);
}

void LightAnimator::buildCustomPalette()
{
  // Gradient palette "grindylow_21_gp", originally from
  // http://soliton.vm.bytemark.co.uk/pub/cpt-city/gacruxa/grindylow/tn/grindylow-21.png.index.html
  // converted for FastLED with gammas (2.6, 2.2, 2.5)
  // Size: 20 bytes of program space.
}

void LightAnimator::ledTurnOff()
{
  for(int i = 0; i < NUM_LEDS; ++i)
  {
    leds_m[i] = CRGB(0,0,0);
  }
  FastLED.show();
}
