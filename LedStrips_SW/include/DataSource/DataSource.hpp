#ifndef ERREUR404_DATA_SOURCE_HPP
#define ERREUR404_DATA_SOURCE_HPP

/**
 * DataSource is a mid-low level layer. It will implement communications
 * and/or driver for the different data sources.
 * It will give access to raw data. The manager will send interpret those datas
 *
 */
class DataSource
{
public:
  /**
   * @brief the default constructor
   */
  DataSource();

  //virtual  ~DataSource();

  enum DataSourceID /// All data sources define by ID
  {
    DATA_SOURCE_SOUND = 0,
    DATA_SOURCE_WIFI,
    DATA_SOUCE_SENSOR,
    DATA_SOURCE_MIDI,
    NB_TOTAL_DATA_SOURCES,
  };

  enum
  {
    ERROR_VALUE = 00
  };

  /**
   * @brief This method will initialize the source
   * @return bool True if everything is ok, false if failure
   */
  virtual bool init() = 0;

  /**
   * @brief Retrieve datas from the source
   * @param pData the pointer that point to the table to be filled with datas
   * @param length The length of the data table
   * @return bool True if datas are reliable, false otherwise
   */
  virtual bool getData(char *pData, int length) = 0;

  /**
   * @brief The tick method will update the datas_m table.
   *        Should be called periodically
   */
  virtual void tick();

protected:
  char* datas_m;

private:
  bool isAvailable_m; //!< Avaialability of the source
};

#endif
