#ifndef ERREUR_404_SOUND_SOURCE_HPP
#define ERREUR_404_SOUND_SOURCE_HPP

#include "DataSource.hpp"

#include <SoftwareSerial.h>

class SoundSource : public DataSource
{
public:
  SoundSource();

  bool init() override;

  bool getData(char* pData, int length) override;

  void tick() override;

  enum
  {
    DATA_BUFFER_LENGTH = 23
  };

private:
  enum
  {
    START_SIGNAL = 0x10,
    END_SIGNAL = 0xFF,
  };

  enum
  {
    BAUD_RATE = 57600,
    RX_PIN = 13,
    TX_PIN = 15,
    BLOCKSIZE = 16,
  };

  SoftwareSerial* swSer;

  bool dataOK_m; //!< Data receiveds are OK
};

#endif
