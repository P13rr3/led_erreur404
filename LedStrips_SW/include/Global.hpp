// ------------- Here some global definitions for the app
#define DEBUG_BAUDRATE 57600
#define LED_PIN_DEBUG_1 2
#define CORE_DATA_TASK 0
#define PIN_INTERRUPT 25


// ------------- Here some declaration for color to use
// Check colors here : http://colorizer.org/
#define HUE_PURPLE 290

#define NUMBER_ANIMATIONS 5
