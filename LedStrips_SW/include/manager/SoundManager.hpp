#ifndef ERREUR_404_SOUND_MANAGER_HPP
#define ERREUR_404_SOUND_MANAGER_HPP

#include "DataSourceManager.hpp"

#include "DataSource/SoundSource.hpp"

/**
 * This class override the sound manager class.
 * It manage all data sound which come from DataSource (For sound)
 *
 */
class SoundManager : public DataSourceManager
{
public:
  /**
   * @brief Default constructor
   */
  SoundManager();

  /**
   * @brief constructor with specified bufferSize
   * @param bufferSize The size of the buffer
   */
  SoundManager(int bufferSize);

  /**
   * @brief This method will return the ID of the SoundManager
   * @return DataSourceID The ID of SoundManager
   */
  DataSource::DataSourceID getID() override;

  /**
   * @brief This method will monitor and calculate sound values from last sound buffer
   *        Should be ticked if the Manager is enabled
   */
  void update() override;

  /**
   * @brief Retrieve the calculated RMS of the recorded sound (in real time, sound from sound buffer)
   * @return The RMS of the sound
   */
  int getRms();

  /**
   * @brief Retrieve the calculated Zero Crossing Rate of the recorded sound (in real time, sound from sound buffer)
   * @return The Zero Crossing Rate of the sound
   */
  int getZcr();

   /**
    * @brief Retrieve the calculated Peak Energy of the recorded sound (in real time, sound from sound buffer)
    * @return The Peak Energy of the sound
    */
   int getPeakNrj();

  /**
   * @brief Retrieve the calculated Spectral Centroid of the recorded sound (in real time, sound from sound buffer)
   * @return The Spectral Centroid of the sound
   */
  int getSpecCentroid();

   /**
    * @brief Retrieve the calculated Spectral Crest of the recorded sound (in real time, sound from sound buffer)
    * @return The Spectral Crest of the sound
    */
   int getSpecCrest();

   /**
    * @brief Retrieve the calculated Spectral Flatness of the recorded sound (in real time, sound from sound buffer)
    * @return The Spectral Flatness of the sound
    */
   int getSpecFlatness();

   /**
    * @brief Retrieve the calculated Roll Off of the recorded sound (in real time, sound from sound buffer)
    * @return The Roll Off of the sound
    */
   int getRollOff();

   /**
    * @brief Retrieve the calculated Spectral Kurtosis of the recorded sound (in real time, sound from sound buffer)
    * @return The Spectral Kurtosis of the sound
    */
   int getSpecKurtosis();

   /**
    * @brief Retrieve the calculated Energy Difference of the recorded sound (in real time, sound from sound buffer)
    * @return The Energy Difference of the sound
    */
   int getNrjDiff();

   /**
    * @brief Retrieve the calculated Spectral Difference of the recorded sound (in real time, sound from sound buffer)
    * @return The Spectral Difference of the sound
    */
   int getSpecDifference();

   /**
    * @brief Retrieve the calculated HF content of the recorded sound (in real time, sound from sound buffer)
    * @return The HF Content of the sound
    */
   int getHfContent();

  /**
   * @brief Retrieve the calculated Pitch of the recorded sound (in real time, sound from sound buffer)
   * @return The pitch of the sound
   */
  int getPitch();

private:
  /**
   * @brief Initialize the SoundManager
   */
  void init() override;

  /**
   * @brief This method will process the datas: it will interpret the datas from source_m
   */
  void processData();

  SoundSource* source_m;  //!< The data source. The provider of raw data

  bool validity_m;        //!< Data valifity

  char rawData_m[SoundSource::DATA_BUFFER_LENGTH];   // Raw data from SoundSource

  int rms_m;            //!< Root Mean Square
  int zcr_m;            //!< Zero Crossing Rate
  int peakNrj_m;        //!< Peak Energy
  int specCentroid_m;   //!< Spectral Centroid
  int specCrest_m;      //!< Spectral Crest
  int specFlatness_m;   //!< Spectral Flatness
  int rollOff_m;        //!< Roll Off
  int specKurtosis_m;   //!< Spectral Kurtosis
  int nrjDiff_m;        //!< Energy difference
  int specDiff_m;       //!< Spectral Difference
  int hfContent_m;      //!< High Frequency Content
  int pitch_m;          //!< Pitch
};

#endif //ERREUR_404_SOUND_MANAGER_HPP
