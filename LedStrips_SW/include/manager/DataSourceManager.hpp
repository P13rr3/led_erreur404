#ifndef ERREUR_404_DATA_SOURCE_MANAGER_HPP
#define ERREUR_404_DATA_SOURCE_MANAGER_HPP

#include "DataSource/DataSource.hpp"

/**
 * The data manager data sources. It gives access to datas, activate or not the source.
 * It will also monitor the sources, by updating the receiving datas.
 * It is a pure virtual class.
 */
class DataSourceManager
{
public:
  /**
   * @brief Default constructor
   *
   */
  DataSourceManager();

  /**
   * @brief Activate or Deacivate the source
   *
   * @param active True if activated, false otherwise
   * @return True if activation/deactivation is effectie, false otherwise.
   *         For example, if the function is called while it's not available, a false will be returned
   */
  bool setActive(bool active);

  /**
   * @brief Method to know the active state of the source
   *
   * @return True if active, false otherwise
   */
  virtual bool isActive();

  /**
   * @brief Retrieve the number of available sources
   *
   * @return The actual number of available sources.
   */
  int getNbAvailableSource();

  /**
   * @brief Retrieve the ID of the current source
   *
   * @return The actual ID of the current source
   */
  virtual DataSource::DataSourceID getID() = 0;

  /**
   * @brief Retrieve the availability of the current source. False by default
   *
   * @return True if available, false otherwise
   */
  virtual bool isAvailable();

  /**
   * @brief This Method will trigger the update for data sources
   *
   */
  virtual void update() = 0;

protected:
  /**
   * @brief Initialize the source
   */
  virtual void init() = 0;

  bool active_m;      //!< The active state of the data source
  bool available_m;   //!< The availability state of the data source

private:

};

#endif //ERREUR_404_DATA_SOURCES_MANAGER_HPP
