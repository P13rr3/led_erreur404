#ifndef LIGHT_ANIMATOR_HPP
#define LIGHT_ANIMATOR_HPP

#include <FastLED.h>


/**
 * The class LightAnimator will process all the animation.
 * It is feed with data from the DataManager. With all these data, the LightAnimator
 * will process the animation and send RGB table value to the strip, and illuminate the world
 *
 * For all methods, see https://platformio.org/lib/show/126/FastLED
 */
class LightAnimator
{
public:

  enum Animation : uint8_t
  {
    ANIMATION_COLOR_TEMPERATURE,
    ANIMATION_CYLON,
    ANIMATION_CONFETTI,
    ANIMATION_SINELON,
    ANIMATION_BPM,
    ANIMATION_FIRE,
    ANIMATION_NOISE,
    ANIMATION_COLOR_PALETTE,
    NO_ANIMATION,
  };

  enum E404ColorPalette : uint8_t
  {
    PALETTE_OCEAN,
    PALETTE_CLOUD,
    PALETTE_LAVA,
    PALETTE_FOREST,
    PALETTE_PARTY,
    PALETTE_GRINDY_LOW,
    PALETTE_PURPLE_FLY,
    PALETTE_FRIDAY_SUNSET,
    PALETTE_FRENCH_FLAG,
    PALETTE_CUSTOM,
    NO_COLOR_PALETTE,
  };

  /**
   * @brief Default constructor
   *
   */
  LightAnimator();

  /**
   * @brief Process the color temperature animation
   *
   * @param colorTemp Predefined FastLed color temperature. Possible value:
   *        Candle, Tungsten40W, Tungsten100W, Halogen, CarbonArc,
   *        HighNoonSun, DirectSunlight, OvercastSky, ClearBlueSky,
   *        WarmFluorescent, StandardFluorescent, CoolWhiteFluorescent,
   *        FullSpectrumFluorescent, GrowLightFluorescent, BlackLightFluorescent,
   *        MercuryVapor, SodiumVapor, MetalHalide, HighPressureSodium, UncorrectedTemperature
   */
  void colorTemp(ColorTemperature colorTemp);

   /**
    * @brief Process the fire animation. Example values : sparking = 120, heatOffset = 200, delay = 10
    *
    * @see https://platformio.org/lib/show/126/FastLED
    */
   void fire(uint16_t rms, uint16_t specCrest);

   /*
    * @brief Process the noise animation (Deprecated)
    *
    */
   void noiseDEP(uint16_t scale, uint16_t speed, uint8_t hueMin, uint8_t hueMax);

   /**
    * @brief Noise function
    *
    */
   void noise(uint16_t zcr, uint16_t specFlat, uint16_t specCentroid, uint8_t minHue, uint8_t maxHue);

  /**
   * @brief Quick method to test different animation
   *
   */
  void animationTest();

  /**
   * @brief Test FastLed math function
   */
  void testMathFunction();

  /**
   * @brief Method that test basic colors of led. Display colors in this order: Red, Green, Blue, Black, White
   *
   */
  void calibratorTest();

  /**
   * @brief Process the color palette animation
   *
   */
  void colorPalette(E404ColorPalette colorP, uint16_t _delay, int rms, int crest);

  /**
   * @brief New movement, interact with the ZCR, spectral centroid and flatness
   *
   */
  void moveTwoOpposite(uint8_t mainHue, uint16_t delay, uint16_t zcr, uint16_t specCentroid, uint8_t specFlatness);

  /**
   * @brief simple strob implementation
   *
   */
  void strob();

  /**
   * @brief Turn all leds to black (Turn off the leds)
   */
   void ledTurnOff();

private:
  enum
  {
    //NUM_LEDS = 57 * 6 ,
    NUM_LEDS = 2 * (57 / 3),
  };
  enum : uint8_t
  {
    DATA_PIN = 14,
    BRIGHTNESS = 255,
    DISPLAYTIME = 20, // How many seconds to show each temperature before switching
    BLACKTIME = 3,    // How many seconds to show black between switches
    COOLING = 55,
  };

  void fadeall(uint8_t scale);
  uint16_t XY( uint8_t x, uint8_t y, uint8_t matrixWidth);

  void buildCustomPalette();

  Animation currentAnimation_m;
  CRGB leds_m[NUM_LEDS];
  uint8_t noise_m[NUM_LEDS][NUM_LEDS];
};

#endif //LIGHT_ANIMATOR_HPP
